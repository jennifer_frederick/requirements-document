Requirements
============

Functional Requirements
-----------------------
#. PFLOTRAN must simulate miscible and immiscible multiphase flow.  The code will accommodate two fluid phases and two components: water and a component that can be air or a pure gas species (e.g. H2(g)).
#. PFLOTRAN can simulate single-phase variably saturated flow if mass conservation in the gas phase is unnecessary.
#. PFLOTRAN must simulate heat transfer that accounts for thermal conduction and convection within the fluid and rock phases.
#. PFLOTRAN must simulate solute transport by accounting for solute advection and hydrodynamic dispersion.
#. PFLOTRAN must simulate the following chemical reactions: (1) radioactive decay and ingrowth with daughter products, (2) sorption through the linear KD model, and (3) mineral precipitation/dissolution.  Radioactive decay must be accounted for in the aqueous, sorbed and precipitate phases.
#. PFLOTRAN must accommodate both structured and unstructured discretization of the problem domain.
#. PFLOTRAN must support the specification of heterogeneous material properties such as permeability, porosity, tortuosity, etc.
#. PFLOTRAN must have the option of calculating fluid density, viscosity, enthalpy and saturation pressure through equations of state published in the literature.
#. PFLOTRAN must support widely used characteristic curves such as the van Genuchten and Brooks-Corey capillary pressure/saturation functions and the Mualem and Burdine relative permeability functions.  The code should have the ability to implement customized characteristic curves if they are warranted.
#. PFLOTRAN must simulate the lifespan of a nuclear waste package/waste form.
#. PFLOTRAN must handle liquid phase dry-out and resaturation across all process models.
#. PFLOTRAN must support specified value, specified flux and zero gradient boundary conditions.
#. PFLOTRAN must support initial conditions specified using external datasets.
#. PFLOTRAN must support time-dependent boundary conditions and source terms.
#. PFLOTRAN must simulate a radionuclide source term dependent on waste form degradation.

Other Requirements
------------------
#. PFLOTRAN must be employ object oriented design to ensure modularity and extensibility.
#. PFLOTRAN must compile and run on a Linux-based desktop computer and a massively parallel supercomputer.
#. PFLOTRAN must employ scalable, binary file IO for reading/writing large data sets on the supercomputer.
#. PFLOTRAN must provide fault tolerance capability.


